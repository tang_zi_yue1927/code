#include<stdio.h>
#include<utmp.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#include<time.h>

void show_info(struct utmp*);
void showtime(long);

int main()
{
    struct utmp current_record;			//定义utmp结构体
    int utmpfd;							//定义文件描述符
    int reclen = sizeof(current_record);//获得utmp结构体大小

	//打开文件的错误处理
    if((utmpfd = open(UTMP_FILE,O_RDONLY)) == -1){
        perror(UTMP_FILE);
        exit(1);
    }

	//读取文件内容并打印
    while(read(utmpfd,&current_record,reclen) == reclen){
        show_info(&current_record);
    }
    close(utmpfd);
    return 0;
}

//打印读取到的结构体数据
void show_info(struct utmp* utbufp)
{
	//当目前记录不是用户信息时，舍弃
    if(utbufp->ut_type != USER_PROCESS){
		return;
	}
	//打印用户名
    printf("%-10.10s",utbufp->ut_name);
    printf(" ");
	//打印用户登录终端
    printf("%-10.10s",utbufp->ut_line);
    printf(" ");
	//打印用户登录时间
    showtime((long)utbufp->ut_time);
	//打印用户登录地址
	if(utbufp->ut_host[0] != '\0'){
		printf("(%s)",utbufp->ut_host);
	}
    printf("\n");
}

//完成时间转换并打印
void showtime(long timeval)
{
    char *cp;
    cp = ctime(&timeval);
    printf("%24.24s",cp);
}
