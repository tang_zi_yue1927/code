#include <gmp.h>
#include <string.h>
#include <stdio.h>
int main(int argc, const char *argv[])
{
    mpz_t z_i, z_s, z_o;
    mpz_init_set_str(z_i, "1", 10);
    mpz_init_set_str(z_s, "1", 10);
    mpz_init_set_str(z_o, "1", 10);
    int i;
    for (i = 0; i < 1327; i++)
    {
        mpz_mul(z_s, z_s, z_i);
        mpz_add(z_i, z_i, z_o);
    }
    gmp_printf("%Zd\n",z_s);
    mpz_clear(z_i);
    mpz_clear(z_s);
    mpz_clear(z_o);
    getchar();
    return 0;
}
