#include <gmp.h>
#include <string.h>
#include <stdio.h>

int main(int argc, const char *argv[])
{
    mpz_t z_r, z_1327, z_1;
    mpz_init_set_str(z_r, "1", 10);
    mpz_init_set_str(z_1327, "20191322", 10);
    mpz_init_set_str(z_1, "1", 10);
    int i;
    for (i = 1322; i <= 1332; i++)
    {
        mpz_mul(z_r, z_r, z_1327);
        mpz_add(z_1327,z_1327,z_1);
    }
    gmp_printf("%Zd\n",z_r);
    mpz_clear(z_r);
    mpz_clear(z_1327);
    mpz_clear(z_1);
    getchar();
    return 0;
}
