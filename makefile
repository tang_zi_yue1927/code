testmymath:add.o sub.o mul.o div.o main.o
	gcc -o testmymath add.o sub.o mul.o div.o main.o 
add.o:add.c head.h
	gcc -c add.c
sub.o:sub.c head.h
	gcc -c sub.c
mul.o:mul.c head.h
	gcc -c mul.c
div.o:div.c head.h
	gcc -c div.c
main.o:main.c head.h
	gcc -c main.c   
clean:
	rm testmymath add.o sub.o mul.o div.o main.o
