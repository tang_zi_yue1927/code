#include <stdio.h>
#include <stdlib.h>
#include <openssl/bn.h>

int main()
{
    BIGNUM *bn;
    bn = BN_new();
    BN_CTX *bn_ctx;
    bn_ctx = BN_CTX_new();
    
    BN_set_word(bn, 20191322);
    
    BIGNUM *b;
    b = BN_new();
    BN_set_word(b, 20191323);

    BN_mul(bn, bn, b, bn_ctx);

    BN_set_word(b, 20191324);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191325);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191326);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191327);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191328);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191329);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191330);
    BN_mul(bn, bn, b, bn_ctx);
    BN_set_word(b, 20191331);
    BN_mul(bn, bn, b, bn_ctx);

    char *a = BN_bn2dec(bn);
    puts(a);
    BN_CTX_free(bn_ctx);
    BN_free(b);
    BN_free(bn);
    return 0;
}
