#include <stdio.h>
#include <stdlib.h>
#include <openssl/bn.h>

int main()
{
    BIGNUM *bn;
    bn = BN_new();
    BN_CTX *bn_ctx;
    bn_ctx = BN_CTX_new();
    
    BN_set_word(bn, 0);
    
    BIGNUM *b;
    b = BN_new();
    BN_set_word(b, 2);
    BIGNUM *c;
    c = BN_new();
    BN_set_word(c, 1327);

    BN_exp(bn, b, c, bn_ctx);

    char *a = BN_bn2dec(bn);
    puts(a);
    BN_CTX_free(bn_ctx);
    BN_free(b);
    BN_free(c);
    BN_free(bn);
    return 0;
}
