#ifndef UTILS_H
#define UTILS_H

int Hex2Char(int fromi,char *toc);//16进制数转换成16进制的字符；
int Char2Hex(char fromc,int *toi);//16进制的字符转化成16进制数；
int BitStr2ByteArr(char *bs,char *ba);//位数组转到字节数组；
int ByteArr2BitStr(char *ba,char *bs);//字节数组转化为位数组；
int INT2ByteArr(int i,char *ba);//整型数转化为字节数组
int ByteArr2INT(char *ba,int *i);//字节数组转化为整型

#endif
